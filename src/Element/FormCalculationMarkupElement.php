<?php

namespace Drupal\calculation_fields\Element;

use Drupal\calculation_fields\CalculationFieldsTrait;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Textfield;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Provide markup element that display the result of an expression.
 *
 * @FormElement("form_calculation_markup")
 */
class FormCalculationMarkupElement extends Textfield {

  use CalculationFieldsTrait;

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#process'][] = self::class . '::processFormCalculationElement';
    $info['#element_validate'][] = self::class . '::validateExpressionValue';
    $info['#evaluation_fields_mask'] = NULL;
    return $info;
  }

  /**
   * Validate form math element callback.
   *
   * To avoid disruptive data from the client, every submit evaluate the
   * expression and set the correct/expected value for the element.
   */
  public static function validateExpressionValue(&$element, FormStateInterface $form_state, &$complete_form) {
    $expressionFields = static::extractEvaluation($element);
    $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);
    if (empty($expressionFields) || empty($value)) {
      return;
    }
    $expressionReplaced = self::buildExpression($expressionFields, $form_state);
    $expressionLanguage = new ExpressionLanguage();
    $result = $expressionLanguage->evaluate($expressionReplaced);
    if ($element["#evaluation_decimals"]) {
      $result = number_format($result, $element["#evaluation_decimals"], '.', '');
    }
    $expectedResult = str_replace('{{ ' . $expressionFields . ' }}', $result, $element["#evaluation_fields"]);
    if ($expectedResult !== $value) {
      $form_state->setValue($element['#name'], $expectedResult);
    }
  }

  /**
   * Process function to get the form context of the element.
   */
  public static function processFormCalculationElement(array &$element, FormStateInterface $form_state, $form) {
    $element['#attributes']['data-evaluate-context'] = str_replace('_', '-', $form["#form_id"]);
    return $element;
  }

  /**
   * Extract evaluation from the element.
   */
  public static function extractEvaluation($element) {
    $pattern = '/{{(.*?)}}/';
    preg_match_all($pattern, $element['#evaluation_fields'], $matches);
    if (empty($matches[1])) {
      return NULL;
    }
    return trim(current($matches[1]));
  }

  /**
   * Pre render markup that add the dataset of the evaluation data.
   */
  public static function preRenderTextfield($element) {
    if (empty($element['#evaluation_fields'])) {
      return $element;
    }
    $expression = static::extractEvaluation($element);
    if (is_null($expression)) {
      \Drupal::logger('calculation_fields')
        ->warning(t('Failed to extract expression of the evaluation_fields value @evaluation_fields', [
          '@name' => $element['name'],
          '@evaluation_fields' => $element['#evaluation_fields'],
        ]));
      return [
        '#markup' => '',
      ];
    }
    $formContext = '';
    if (isset($element["#attributes"]["data-evaluate-context"])) {
      $formContext = $element["#attributes"]["data-evaluate-context"];
    }
    $textExpression = str_replace('{{ ' . $expression . ' }}', '[RESULT]', $element['#evaluation_fields']);

    return [
      '#theme' => 'form_calculation_element_template',
      '#decimals' => $element['#evaluation_decimals'] ?? NULL,
      '#formContext' => $formContext,
      '#fields' => self::getFieldsFromExpression($expression),
      '#expression' => $expression,
      '#placeholder' => $element['#placeholder'] ?? NULL,
      '#textExpression' => $textExpression,
      '#name' => $element["#name"] ?? $element["#webform_key"],
      '#inputmask' => $element["#evaluation_fields_mask"] ?? NULL,
    ];
  }

}
