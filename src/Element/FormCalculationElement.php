<?php

namespace Drupal\calculation_fields\Element;

use Drupal\calculation_fields\CalculationFieldsTrait;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Number;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Provide a custom form element that display a result of an expression.
 *
 * @FormElement("form_calculation_element")
 */
class FormCalculationElement extends Number {

  use CalculationFieldsTrait;

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#evaluation_fields'] = NULL;
    $info['#evaluation_fields_mask'] = NULL;
    $info['#process'] = [self::class . '::processFormCalculationElement'];
    $info['#element_validate'] = [self::class . '::validate'];
    return $info;
  }

  /**
   * Validate form math element callback.
   *
   * To avoid disruptive data from the client, every submit evaluate the
   * expression and set the correct/expected value for the element.
   */
  public static function validate(&$element, FormStateInterface $form_state, &$complete_form) {
    $expression = static::buildExpression($element["#evaluation_fields"], $form_state);
    $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);
    if (empty($expression) || empty($value)) {
      return;
    }
    $expressionLanguage = new ExpressionLanguage();
    // Mathjs supports pow functions by the char ^
    // symfony/expression-languages support by **
    // Replace them before the php evaluation.
    $expression = str_replace('^', '**', $expression);
    $result = $expressionLanguage->evaluate($expression);
    if (!empty($element["#evaluation_decimals"])) {
      $result = number_format($result, $element["#evaluation_decimals"]);
    }
    if ($result !== $value) {
      $form_state->setValue($element["#name"], $result);
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function preRenderNumber($element) {
    if (empty($element["#evaluation_fields"])) {
      return $element;
    }
    $element['#attributes']['type'] = 'text';
    $element['#attributes']['readonly'] = 'readonly';
    Element::setAttributes($element, ['id', 'name', 'value', 'placeholder', 'size', 'fields']);
    static::setAttributes($element, ['form-number', 'js-form-math-element']);
    $element['#attributes']['data-evaluate-expression'] = $element["#evaluation_fields"];
    $element['#attributes']['data-evaluate-fields'] = static::getFieldsFromExpression($element["#evaluation_fields"]);
    $element['#attached']['library'][] = 'calculation_fields/calculation_fields';

    if (!empty($element["#evaluation_fields_mask"])) {
      $element['#attributes']['data-inputmask'] = $element["#evaluation_fields_mask"];
    }

    return $element;
  }

  /**
   * Process each form calculation element setting custom properties.
   *
   * Set two properties:
   * - data-evaluate-context: used to guarantee that the input will use values,
   * in the correct form context.
   * - data-evaluate-deep-fields: deep fields properties is used to store
   * values of the fields that are not being displayed like in a multistep
   * form in a webform for example.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   * @param array $form
   *   The form items.
   */
  public static function processFormCalculationElement(array $element, FormStateInterface $form_state, array $form) {
    if (empty($element["#evaluation_fields"])) {
      return $element;
    }
    $element['#attributes']['data-evaluate-context'] = str_replace('_', '-', $form["#form_id"]);
    if (isset($element['#evaluation_decimals'])) {
      $element['#attributes']['data-evaluate-decimals'] = (int) $element['#evaluation_decimals'];
    }
    $dependencyFields = static::getFieldsFromExpression($element["#evaluation_fields"]);

    $hasMultipleSteps = $form["progress"]["#current_page"] ?? NULL;

    $dependenciesValues = [];
    foreach ($dependencyFields as $field) {
      if (!empty($form['elements'])) {
        $dependentElement = self::getElement($form['elements'], $field);
      }
      else {
        $dependentElement = self::getElement($form, $field);
      }

      if (isset($dependentElement["#webform"]) &&
        ($dependentElement["#webform_parent_key"] !== $hasMultipleSteps || !$dependentElement['#access'])) {
        $dependenciesValues[$field] = $form_state->getValue($field);
      }
    }
    $element['#attributes']['data-evaluate-deep-fields'] = json_encode($dependenciesValues);
    return $element;
  }

}
