<?php

namespace Drupal\calculation_fields;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Helper trait to manipulate expressions info in calculation elements.
 */
trait CalculationFieldsTrait {

  /**
   * Extract info from the expression field.
   *
   * @param string $field
   *   Field expression info.
   *
   * @return array
   *   Array with field name and default value if defined.
   */
  public static function getExpressionInfo($field) {
    $defaultValue = NULL;
    $info = explode('|', $field);
    if (count($info) === 2) {
      $defaultValue = $info[1];
    }
    return [
      $info[0],
      $defaultValue,
    ];
  }

  /**
   * Get reference to first element by name.
   *
   * @param array $elements
   *   An associative array of elements.
   * @param string $name
   *   The element's name.
   *
   * @return array|null
   *   Reference to found element.
   */
  public static function &getElement(array &$elements, $name) {
    foreach (Element::children($elements) as $element_name) {
      if ($element_name === $name) {
        return $elements[$element_name];
      }
      elseif (is_array($elements[$element_name])) {
        $child_elements = &$elements[$element_name];
        if ($element = &static::getElement($child_elements, $name)) {
          return $element;
        }
      }
    }
    $element = NULL;
    return $element;
  }

  /**
   * Get fields from expression getting the field names.
   *
   * @param string $evaluationFields
   *   Evaluation field expression.
   *
   * @return array
   *   An array with the field names.
   */
  public static function getFieldsFromExpression($evaluationFields): array {
    if (empty($evaluationFields)) {
      return [];
    }
    $expressionFields = explode(':', $evaluationFields) ?? [];
    $expressionFields = array_filter($expressionFields);
    $fields = [];
    foreach ($expressionFields as $field) {
      $items = explode(' ', $field);
      $items = array_map(function ($item) {
        return preg_replace("/[^a-zA-Z0-9_|]/", "", $item);
      }, $items);
      $filteredItems = array_filter($items, function ($item) {
        return !is_numeric($item) && !empty($item);
      });
      $fields = array_merge($fields, $filteredItems);
    }
    return array_unique($fields);
  }

  /**
   * Build expression field replacing field name for this value.
   *
   * Example: (:field_name_1 + :field_name2) became (2 + 2).
   *
   * @param string $evaluation
   *   The field expression.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return string
   *   The expression with values.
   */
  public static function buildExpression($evaluation, FormStateInterface $form_state) {
    $fields = static::getFieldsFromExpression($evaluation);
    $collectedValues = [];
    foreach ($fields as $field) {
      [$fieldName, $defaultValue] = self::getExpressionInfo($field);
      $value = $form_state->getValue($fieldName);
      if (!empty($value)) {
        $collectedValues[$field] = $value;
      }
      elseif (!is_null($defaultValue)) {
        $collectedValues[$field] = $defaultValue;
      }
    }
    if (count($collectedValues) !== count(array_unique($fields))) {
      return NULL;
    }
    $exp = $evaluation;
    foreach ($collectedValues as $field => $value) {
      $exp = str_replace(':' . $field, $value, $exp);
    }
    return $exp;
  }

}
