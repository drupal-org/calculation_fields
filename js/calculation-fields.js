/**
 * @file
 * Behaviors for the form math custom element.
 */

((Drupal, math) => {
  Drupal.behaviors.formCalculationElement = {
    attach: () => {
      const previousValue = {};

      const updateResult = (el, value) => {
        if (el.tagName === 'INPUT') {
          el.value = numberFormat(value, el.dataset.inputmask);
          el.dispatchEvent(new Event('change'));
          return;
        }
        const markupEl = el.querySelector('.form-math-markup-element');
        if (markupEl === null) {
          return;
        }

        const expressionEvaluated = markupEl
          .querySelector('.js-math-result-base')
          .innerHTML.replace('[RESULT]', numberFormat(value, markupEl.dataset.inputmask));

        markupEl.querySelector('.js-math-result-input').value = expressionEvaluated;
        markupEl.querySelector('.js-math-result').innerHTML = expressionEvaluated;
        if (value !== "") {
          markupEl.classList.add('form-math-markup-element-calculated');
        }
        else {
          markupEl.classList.remove('form-math-markup-element-calculated');
        }
      }

      const calc = (fieldElement) => {
        const fields = fieldElement.dataset.evaluateFields.split(' ');
        const expression = fieldElement.dataset.evaluateExpression;
        const formContext = fieldElement.dataset.evaluateContext;
        const deepFields = JSON.parse(fieldElement.dataset.evaluateDeepFields || "{}");
        const fieldEvaluateDecimals = fieldElement.dataset.evaluateDecimals || null;
        const buildExpression = (exp, field) => {
          const [fieldName, defaultValue] = field.split('|');
          const currentElement = document.querySelector(`input[name="${fieldName}"], select[name="${fieldName}"]`);
          let currentFieldValue;
          if (currentElement === null && deepFields[fieldName] !== undefined) {
            currentFieldValue = deepFields[fieldName];
          }
          else {
            currentFieldValue = currentElement.value;
            if (currentFieldValue === "" && defaultValue !== undefined) {
              currentFieldValue = defaultValue
            }
          }
          let fieldExp = fieldName;
          if (defaultValue !== undefined) {
            fieldExp += `|${defaultValue}`;
          }
          if (currentFieldValue !== '') {
            return exp.replaceAll(`:${fieldExp}`, currentFieldValue);
          }
          return exp;
        }

        const handleFieldChange = (e) => {
          // If the value was clear. Rollback the element to the initial state.
          if (e.target.value === "" && previousValue[e.target.name] !== undefined && previousValue[e.target.name] !== "") {
            updateResult(fieldElement, "");
          }

          const convertedExpression = fields.reduce(buildExpression, expression);
          if (convertedExpression.split(':').length === 1) {
            let expressionResult = math.evaluate(convertedExpression);
            if (fieldEvaluateDecimals !== null) {
              expressionResult = parseFloat(expressionResult).toFixed(fieldEvaluateDecimals)
            }
            updateResult(fieldElement, expressionResult);
          }
          previousValue[e.target.name] = e.target.value;
        }

        let selectorFormContext = '';
        if (formContext !== '') {
          selectorFormContext = `form#${formContext} `;
        }
        fields.forEach(fieldExp => {
          const [name] = fieldExp.split('|');
          const elementSelector = `${selectorFormContext}input[name="${name}"], select[name="${name}"]`;
          const element = document.querySelector(elementSelector);
          if (element) {
            element.removeEventListener('change', handleFieldChange);
            element.addEventListener('change', handleFieldChange);
          }
        });
      }

      const numberFormat = (value, format = 'none') => {
        let formatedNUmber = 0;
        const lang = drupalSettings.path.currentLanguage ?? 'en';

        switch(format) {
          case 'currency':
            formatedNUmber = new Intl.NumberFormat(lang, { style: format, currency: 'USD' }).format(value);
            break;
          case 'numeric':
            formatedNUmber = new Intl.NumberFormat().format(value);
          break;
          default:
            formatedNUmber = value;
        }
        return formatedNUmber;
      }

      const triggerOpenedForm = (fields) => {
        const changeEvent = new Event('change');
        fields.forEach((field) => {
          const [fieldName, defaultValue] = field.split('|');
          if (defaultValue !== undefined) {
            const input = document.querySelector(`[name~="${fieldName}"]`);
            if (input) {
              input.dispatchEvent(changeEvent);
            }
          }
        });
      }

      const formCalculationElements = document
        .querySelectorAll('.js-form-math-element:not(.built)');
      if (formCalculationElements.length === 0) {
        return;
      }

      formCalculationElements.forEach(el => {
          if (
            el.dataset.evaluateFields !== undefined &&
            el.dataset.evaluateExpression !== undefined
          ) {
            try {
              calc(el);
              triggerOpenedForm(el.dataset.evaluateFields.split(' '));
            }
            catch (e) {
              console.error(`Failed to calculate ${e}`);
            }
          }
        });
    },
  }
})(Drupal, math);
