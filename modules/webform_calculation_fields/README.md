# Webform Calculation Fields

## What the Module Does
The "Webform Calculation Fields" module is an extension for Drupal's Webform module, designed to enhance the capabilities of webforms by introducing a custom field type called "Calculation Field." This field allows you to perform dynamic calculations within your webforms, similar to the "form_calculation_element" module, while benefiting from Webform's flexibility and user-friendly interface.

## How to Use the Custom Calculation Field in Webforms

### 1. Installing and Enabling the Module
Before using the custom "Calculation Field" field, make sure you've installed and enabled the "Webform Calculation Fields" module. You can do this via the Drupal admin interface.

### 2. Creating a Webform
Create a new webform or edit an existing one, depending on your requirements.

### 3. Adding a Calculation Field
To add a Calculation field to your webform, follow these steps:

1. In your webform, navigate to the "Add Element" section.
2. Look for the "Calculation Field" element type, which is provided by the "Webform Calculation Fields" module.
3. Click on the "Calculation Field" element to add it to your webform.
### 4. Configuring the Calculation Field
Once you've added the Calculation field to your webform, you'll need to configure it. Here are the essential settings:

- **Title**: Give the field a descriptive title.
- **Calculation Formula**: In this field, you can specify the mathematical expression you want to use for the calculation. You can use the same format as in the "form_calculation_element" module, with field names enclosed in colons, such as `:field1 + :field2`. You can reference any other fields in the webform using colons.
- **Decimal Places**: Choose the number of decimal places to round the result to.

### 5. Permissions
The "Webform Calculation Fields" module provides permissions to control who can create, edit, and delete elements of the "Calculation" type. Make sure to configure these permissions according to your site's needs.

### 6. Saving and Using the Webform
After configuring the Calculation field, save your webform, and then it's ready to use. When users fill out the webform, the Calculation field will automatically evaluate the specified formula and display the result.

## Example 1: Simple Calculator
Suppose you're creating a simple webform where users can input two numbers, and you want to display the sum of those numbers. Here's how you can configure the Calculation field:

- Title: "Result"
- Calculation Formula: `:number1 + :number2`
- Decimal Places: 2

In this example, ":number1" and ":number2" should be replaced with the actual field names you've defined in your webform. When users fill in these fields, the "Result" field will automatically calculate and display the sum.

## Example 2: Loan Repayment Calculator
Imagine you're creating a webform for a loan application, and you want to display the monthly loan repayment amount based on the loan amount, interest rate, and loan term. Here's how you can configure the Calculation field:

- Title: "Monthly Payment"
- Calculation Formula: `(:loan_amount * :interest_rate * (1 + :interest_rate) ^ :loan_term) / ((1 + :interest_rate) ^ :loan_term - 1)`
- Decimal Places: 2

In this example, ":loan_amount," ":interest_rate," and ":loan_term" should be replaced with the actual field names you've defined in your webform. The "Monthly Payment" field will automatically calculate and display the monthly repayment amount based on the user's input.

With the "Webform Calculation Fields" module, you can easily incorporate dynamic calculations into your webforms, making them more interactive and user-friendly.
