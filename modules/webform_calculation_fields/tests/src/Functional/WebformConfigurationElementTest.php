<?php

namespace Drupal\Tests\webform_calculation_fields\Functional;

/**
 * Test the form calculation element configuration.
 */
class WebformConfigurationElementTest extends WebformCalculationFieldElementBase {

  /**
   * Test a webform creation using the form calculation element.
   */
  public function testConfigurationElement() {
    $page = $this->getSession()->getPage();
    $this->setPage($page);
    $this->createWebform('Calculation BMI example');

    $this->drupalGet("/admin/structure/webform/manage/calculation_bmi_example/element/add/number");
    $this->createField('Weight', 'number', ['required' => TRUE]);
    $this->createField('Height', 'number', ['required' => TRUE]);

    $this->drupalGet("/admin/structure/webform/manage/calculation_bmi_example/element/add/form_calculation_element");
    $page->findField('title')->setValue('BMI');
    $page->find('css', 'textarea[name="properties[evaluation_fields]"]')->setValue(':weight / (:height ^ 2)');
    $page->find('css', 'input[name="properties[evaluation_decimals]"]')->setValue('2');
    $page->findButton('Save')->click();
    $this->assertSession()->pageTextContains('BMI has been created.');

    $this->drupalGet('/webform/calculation_bmi_example');
    $page->findField('edit-weight')->setValue('75');
    $page->findField('edit-height')->setValue('1.80');
    $this->assertEquals('23.15', $page->findField('edit-bmi')->getValue());
    $page->findButton('Submit')->click();
    $this->assertSession()->pageTextContains('New submission added to Calculation BMI example.');

    $this->drupalGet('/admin/structure/webform/manage/calculation_bmi_example/submission/1/edit');
    $this->assertEquals("23.15", $page->findField('edit-bmi')->getValue());
  }

  /**
   * Test form calculation element validation.
   */
  public function testCalculationElementValidation() {
    $page = $this->getSession()->getPage();
    $this->setPage($page);
    $this->createWebform('Calculation elements validation');
    $this->createField('Field 1', 'number');

    $this->drupalGet("/admin/structure/webform/manage/calculation_elements_validation/element/add/form_calculation_element");
    $page->findField('title')->setValue('Calculation element');
    $page->find('css', 'textarea[name="properties[evaluation_fields]"]')->setValue(':field_x * 10');
    $page->findButton('Save')->click();
    $this->assertSession()
      ->pageTextContains('The following field(s) (field_x) does not exists on the webform. Please check your expression!');
    $page->find('css', 'textarea[name="properties[evaluation_fields]"]')->setValue(':field_1 * 10');
    $page->findButton('Save')->click();
    $this->assertSession()
      ->pageTextContains('The field field_1 is not required and is present on the expression, you must provide a default value, eg: field_1|0 or field_1|1');
    $page->find('css', 'textarea[name="properties[evaluation_fields]"]')
      ->setValue(':field_1|1 * 10');
    $page->findButton('Save')->click();
    $this->assertSession()->pageTextContains('Calculation element has been created.');

    // Test the expression using the default valued defined to the field.
    $this->drupalGet('/webform/calculation_elements_validation');
    $this->assertEquals('10', $page->findField('edit-calculation-element')->getValue());
    $page->findField('edit-field-1')->setValue('20');
    $this->assertEquals('200', $page->findField('edit-calculation-element')->getValue());
    $page->findField('edit-field-1')->setValue('');
    $this->assertEquals('10', $page->findField('edit-calculation-element')->getValue());
    $page->findButton('Submit')->click();
    $this->assertSession()->pageTextContains('New submission added to Calculation elements validation.');

    $this->drupalGet('/admin/structure/webform/manage/calculation_elements_validation/submission/1/edit');
    $this->assertEquals("", $page->findField('edit-field-1')->getValue());
    $this->assertEquals("10", $page->findField('edit-calculation-element')->getValue());
  }

  /**
   * Test mask feature with form calculation elements.
   */
  public function testCalculationElementMask() {
    $this->page = $this->getSession()->getPage();
    $page = $this->page;
    $this->createWebform('Calculation elements mask');
    $this->createField('Total amount', 'number', ['required' => TRUE]);

    $this->createField('Total after taxes', 'form_calculation_element', [
      'el_settings' => [
        'evaluation_fields' => ':total_amount - (:total_amount * 0.27)',
        'evaluation_decimals' => '2',
        'evaluation_fields_mask' => 'currency',
      ],
    ]);

    $this->drupalGet('/webform/calculation_elements_mask');
    $page->findField('edit-total-amount')->setValue("10000");
    $this->assertEquals('currency', $page->findField('edit-total-after-taxes')->getAttribute('data-inputmask'));

    $this->drupalGet('/admin/structure/webform/manage/calculation_elements_mask/element/total_after_taxes/edit');
    $page->find('css', 'select[name="properties[evaluation_fields_mask]"]')->setValue('numeric');
    $page->findButton('Save')->click();

    $this->drupalGet('/webform/calculation_elements_mask');
    $page->findField('edit-total-amount')->setValue("10000");
    $this->assertEquals('numeric', $page->findField('edit-total-after-taxes')->getAttribute('data-inputmask'));
  }

}
