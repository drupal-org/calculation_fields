<?php

namespace Drupal\webform_calculation_fields\Plugin\WebformElement;

use Drupal\calculation_fields\CalculationFieldsTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\Number;

/**
 * Provides a Webform Math element.
 *
 * @WebformElement(
 *   id = "form_calculation_element",
 *   label = @Translation("Calculation Field"),
 *   description = @Translation("Provides a math element capable to evaluate math expressions."),
 *   category = @Translation("Calculation elements"),
 * )
 */
class WebformCalculationNumber extends Number {

  use CalculationFieldsTrait;

  /**
   * {@inheritDoc}
   */
  public function defineDefaultProperties() {
    return parent::defineDefaultProperties() + [
      'evaluation_fields' => '',
      'evaluation_decimals' => NULL,
      'evaluation_fields_mask' => '',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['evaluation_decimals'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Round result'),
      '#description' => $this->t('Specify the number of decimal places for the calculation result'),
      '#weight' => 1,
    ];

    $description = $this->t('e.g.:
    :webform_element_key_1 + :webform_element_key_2 - (:webform_element_key_1 * :webform_element_key_3).
    For non required field you must provide a default value to be used on the expression until the field be populated, just adding |defaultValue, like :my_field_name|0');

    $form['element']['evaluation_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Math Expression'),
      '#description' => $description,
      '#weight' => 2,
      '#required' => TRUE,
    ];

    // Evaluation Fields Mask.
    $form['element']['evaluation_fields_mask'] = [
      '#type' => 'select',
      '#title' => $this->t('Mask'),
      '#description' => $this->t("Select the mask for the element's output. Choose between None, Numeric (9.999.999), and Currency ($9.99)."),
      '#weight' => 3,
      '#required' => FALSE,
      '#options' => [
        'none' => $this->t('None'),
        'numeric' => $this->t('Numeric (9.999.999)'),
        'currency' => $this->t('Currency ($9.99)'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $expression = $form_state->getValue('evaluation_fields');
    $fields = self::getFieldsFromExpression($expression);
    $currentWebform = $this->getWebform();
    $invalidFields = [];
    foreach ($fields as $field) {
      [$fieldName, $defaultValue] = self::getExpressionInfo($field);
      $element = $currentWebform->getElement($fieldName);
      if (!$element) {
        $invalidFields[] = $fieldName;
      }
      elseif (!$element['#required'] && is_null($defaultValue)) {
        $form_state
          ->setErrorByName(
            'evaluation_fields',
            $this->t('The field @field is not required and is present on the expression, you must provide a default value, eg: @field|0 or @field|1', [
              '@field' => $fieldName,
            ])
                );
      }
    }
    if (!empty($invalidFields)) {
      $form_state
        ->setErrorByName(
          'evaluation_fields',
          $this->t(
            'The following field(s) (@fields) does not exists on the webform. Please check your expression!',
            ['@fields' => implode(', ', $invalidFields)]
          )
        );
    }
    parent::validateConfigurationForm($form, $form_state);
  }

}
