<?php

namespace Drupal\webform_calculation_fields\Plugin\WebformElement;

use Drupal\calculation_fields\CalculationFieldsTrait;
use Drupal\calculation_fields\Element\FormCalculationElement;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\TextField;

/**
 * Provides a Webform calculation element.
 *
 * @WebformElement(
 *   id = "form_calculation_markup",
 *   label = @Translation("Calculation Markup"),
 *   description = @Translation("Provided an advance markup that can evaluate expression inside {{ }} tags."),
 *   category = @Translation("Calculation elements"),
 * )
 */
class WebformCalculationMarkup extends TextField {

  use CalculationFieldsTrait;

  /**
   * {@inheritDoc}
   */
  public function defineDefaultProperties() {
    $default = parent::defineDefaultProperties();
    $custom = [
      'evaluation_decimals' => NULL,
      'evaluation_fields' => '',
      'evaluation_fields_mask' => '',
      'placeholder' => '',
    ];
    return array_merge(
      $default,
      $custom
    );
  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    unset($form["summary_attributes"]);
    unset($form["validation"]);
    unset($form["element_description"]);
    unset($form["form"]);

    $form['element']['evaluation_fields'] = [
      '#type' => 'webform_html_editor',
      '#title' => $this->t('Math Expression and the Html markup'),
      '#description' => $this->t('e.g.: &lt;h3&gt;:webform_element_key_1 + :webform_element_key_2 - (:webform_element_key_1 * :webform_element_key_3)&lt;/h3&gt;'),
      '#required' => TRUE,
    ];

    $form['element']['evaluation_decimals'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Decimals'),
      '#description' => $this->t('Specify the number of decimal places for the calculation result'),
      '#weight' => 1,
    ];

    $form['element']['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#description' => $this->t('The text to be displayed until all inputs required by this field are populated. If not provided, the markup element will only be displayed once the calculation is complete.'),
      '#weight' => 2,
    ];

    // Evaluation Fields Mask.
    $form['element']['evaluation_fields_mask'] = [
      '#type' => 'select',
      '#title' => $this->t('Mask'),
      '#description' => $this->t("Select the mask for the element's output. Choose between None, Numeric (9.999.999), and Currency ($9.99)."),
      '#weight' => 3,
      '#required' => FALSE,
      '#options' => [
        'none' => $this->t('None'),
        'numeric' => $this->t('Numeric (9.999.999)'),
        'currency' => $this->t('Currency ($9.99)'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $expression = strip_tags($form_state->getValue('evaluation_fields'));
    $pattern = '/{{(.*?)}}/';
    preg_match_all($pattern, $expression, $matches);
    if (empty($matches[1])) {
      $form_state
        ->setErrorByName(
          'evaluation_fields',
          $this->t('Failed to extract expression, make sure to round the expression between {{ }}. eg: {{ :field1 + :field2 }}')
        );
      return parent::validateConfigurationForm($form, $form_state);
    }

    $fields = FormCalculationElement::getFieldsFromExpression($expression);
    $currentWebform = $this->getWebform();
    $invalidFields = [];
    foreach ($fields as $field) {
      [$fieldName, $defaultValue] = self::getExpressionInfo($field);
      $element = $currentWebform->getElement($fieldName);
      if (!$element) {
        $invalidFields[] = $fieldName;
      }
      elseif (!$element['#required'] && is_null($defaultValue)) {
        $form_state
          ->setErrorByName(
            'evaluation_fields',
            $this->t('The field @field is not required and is present on the expression, you must provide a default value, eg: @field|0 or @field|1', [
              '@field' => $fieldName,
            ])
          );
      }
    }
    if (!empty($invalidFields)) {
      $form_state
        ->setErrorByName(
          'evaluation_fields',
          $this->t('The following field(s) (@fields) does not exists on the webform. Please check your expression!',
            ['@fields' => implode(', ', $invalidFields)]
          )
        );
    }
    return parent::validateConfigurationForm($form, $form_state);
  }

}
