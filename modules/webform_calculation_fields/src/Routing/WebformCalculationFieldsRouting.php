<?php

namespace Drupal\webform_calculation_fields\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\webform_calculation_fields\Form\WebformCalculationFieldsForm;
use Drupal\webform_calculation_fields\Form\WebformCalculationFieldsFormDelete;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscribe alter route event.
 *
 * Update the routes defined adding new form that validate if the user can
 * do anything with the form element.
 */
class WebformCalculationFieldsRouting extends RouteSubscriberBase {

  /**
   * Routes to set the new form behavior.
   *
   * @var string[]
   */
  protected static $routes = [
    'entity.webform_ui.element.edit_form' => WebformCalculationFieldsForm::class,
    'entity.webform_ui.element.delete_form' => WebformCalculationFieldsFormDelete::class,
  ];

  /**
   * {@inheritDoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach (static::$routes as $routeName => $form) {
      $routeInfo = $collection->get($routeName);
      if ($routeInfo) {
        $routeInfo->setDefault('_form', $form);
      }
    }
  }

}
