<?php

namespace Drupal\webform_calculation_fields\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\webform\WebformInterface;

/**
 * Provide access feature for the custom math element.
 */
trait WebformCalculationFieldsElementUiAccess {

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformInterface $webform = NULL, $key = NULL, $parent_key = NULL, $type = NULL) {
    $router_match = $this->getRouteMatch();
    $field_key = $router_match->getParameter('key');
    $field_props = $webform->getElement($field_key);
    $element_type = $field_props['#webform_plugin_id'];
    $uid = $this->currentUser()->id();
    if ($element_type === 'form_calculation_element' && !User::load($uid)->hasPermission('administer calculation_fields configuration')) {
      return [
        "#type" => "markup",
        "#markup" => t('You do not have access to delete this field.'),
      ];
    }
    return parent::buildForm($form, $form_state, $webform, $key, $parent_key, $type);
  }

}
