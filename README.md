# Calculation Fields

## What the Module Does
+The "calculation_fields" module is designed to enhance Drupal forms by introducing a new form element type called "form_calculation_element." This custom form element allows you to create dynamic and mathematically calculated fields within your forms. It's particularly useful when you need to perform mathematical operations based on the values of other form fields and update the result in real-time.

## How to Use the New Element

### 1. Adding the "form_calculation_element" to a Form
To use the "form_calculation_element" in your Drupal form, you need to define it in the form array. Here's a basic example of how to do this:

```php
$form['result'] = [
  '#type' => 'form_calculation_element',
  '#title' => $this->t('Result'),
  '#required' => TRUE,
  '#evaluation_fields' => '(:first_value + :second_value + :third_value) * :multiple',
  '#evaluation_decimals' => 2,
];
```

In this example, we create a field named 'result' of type 'form_calculation_element.' It has the following attributes:
- `#title`: The title of the field.
- `#required`: Whether this field is mandatory.
- `#evaluation_fields`: The expression to be evaluated. In this case, it calculates the result by adding `:first_value`, `:second_value`, and `:third_value`, and then multiplying the result by `:multiple`. You can use field names with `:` to reference the values of other form fields, to set a default value of the field until be populated just add `:field_name|0` or `:field_name|1` for division and multiplication. When any of these fields change, the expression is re-evaluated and updates the 'result' field.
- `#evaluation_decimals`: The number of decimal places to round the result to.


### 2. Examples of Usage

**Example 1: Calculate Total Price**
Suppose you have a form for ordering items, and you want to calculate the total price based on the quantity and unit price. You can use the "form_calculation_element" as follows:

```php
$form['quantity'] = [
  '#type' => 'textfield',
  '#title' => $this->t('Quantity'),
];
$form['unit_price'] = [
  '#type' => 'textfield',
  '#title' => $this->t('Unit Price'),
];
$form['total_price'] = [
  '#type' => 'form_calculation_element',
  '#title' => $this->t('Total Price'),
  '#evaluation_fields' => ':quantity * :unit_price',
  '#evaluation_decimals' => 2,
];
```

Now, when users enter the quantity and unit price, the total price is automatically calculated and displayed.

**Example 2: Calculate BMI (Body Mass Index)**
Imagine you have a health assessment form, and you want to calculate the Body Mass Index (BMI) based on a person's weight and height. You can achieve this with the "form_calculation_element" element:

```php
$form['weight'] = [
  '#type' => 'textfield',
  '#title' => $this->t('Weight (kg)'),
];
$form['height'] = [
  '#type' => 'textfield',
  '#title' => $this->t('Height (m)'),
];
$form['bmi'] = [
  '#type' => 'form_calculation_element',
  '#title' => $this->t('BMI'),
  '#evaluation_fields' => ':weight / (:height * :height)',
  '#evaluation_decimals' => 2,
];
```

Now, as users input their weight and height, the BMI is calculated and displayed, providing immediate health feedback.

With the "form_calculation_element" module, you can easily incorporate dynamic calculations into your forms, making them more interactive and informative for users.
