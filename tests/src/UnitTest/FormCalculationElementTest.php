<?php

namespace Drupal\Tests\calculation_fields\UnitTest;

use Drupal\calculation_fields\Element\FormCalculationElement;
use Drupal\Core\Form\FormState;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the FormCalculationElement class.
 *
 * @coversDefaultClass \Drupal\calculation_fields\Element\FormCalculationElement
 *
 * @group calculation_fields
 */
class FormCalculationElementTest extends UnitTestCase {

  /**
   * Tests the getFieldsFromExpression method.
   *
   * @dataProvider getFieldsFromExpressionDataProvider
   */
  public function testGetFieldsFromExpression($inputExpression, $expectedFields) {
    $result = FormCalculationElement::getFieldsFromExpression($inputExpression);
    $this->assertEquals($expectedFields, $result);
  }

  /**
   * Provides data for the testgetFieldsFromExpression method.
   */
  public function getFieldsFromExpressionDataProvider() {
    return [
      // Test simple math expressions.
      [':field1 + :field2', ['field1', 'field2']],
      [':field1 - :field2', ['field1', 'field2']],
      [':field1 * :field2', ['field1', 'field2']],
      [':field1 / :field2', ['field1', 'field2']],
      [':field1|2 / :field2', ['field1|2', 'field2']],
      // Test advanced math expressions.
      ['(:field1 + :field2) / :field3', ['field1', 'field2', 'field3']],
      [':field1 - :field2 + :field3 * :field4', ['field1', 'field2', 'field3', 'field4']],
      ['(:field1 * :field2) / ((:field3 * :field4) / :field1)', ['field1', 'field2', 'field3', 'field4']],
    ];
  }

  /**
   * Test FormCalculationElement::buildExpression.
   *
   * @dataProvider buildExpressionDataProvider
   */
  public function testBuildExpression($expression, $formValues, $expectedExpression) {
    $formState = new FormState();
    foreach ($formValues as $field => $value) {
      $formState->setValue($field, $value);
    }
    $result = FormCalculationElement::buildExpression($expression, $formState);
    $this->assertEquals($result, $expectedExpression);
  }

  /**
   * Provide test for testBuildExpression.
   */
  public function buildExpressionDataProvider() {
    return [
      [':field1 + :field2', ['field1' => 1, 'field2' => 2], '1 + 2'],
      [':field1 * :field2', ['field1' => 1, 'field2' => 2], '1 * 2'],
      [':field1 - :field2', ['field1' => 1, 'field2' => 2], '1 - 2'],
      [':field1 / :field2', ['field1' => 1, 'field2' => 2], '1 / 2'],
      ['(:field1 / :field2) * :field3', ['field1' => 1, 'field2' => 2, 'field3' => 3], '(1 / 2) * 3'],
      [':field1|2 / :field2', ['field2' => 2], '2 / 2'],
    ];
  }

}
